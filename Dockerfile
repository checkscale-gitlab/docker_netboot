# choosing base image
FROM alpine:3.15

# installing required packages
RUN apk add --no-cache dnsmasq syslinux apache2 p7zip

# copying files to container
COPY root/ /

# declaring volume for data
VOLUME ["/storage"]

# choosing system starting point
ENTRYPOINT ["/init.sh"]
